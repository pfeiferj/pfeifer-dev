---
title: "Considerations When Building an App"
date: "2020-06-27"
---

* Build System (typescript vs pure js)
* Testing System (jest vs mocha)
* End to End Testing (cypress)
* ORM vs query builder (sequelize vs knex)
* Migration strategy (sequelize vs knex)
* Code Structure (micro services vs monolithic)
* Deployment Strategy (bare metal vs docker vs kubernetes)
